<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'aurorens_bd');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'aurorens_usu');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'aur2000');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xn>malBu@>|2!b6,>C/Eli5RB^#cZm4Bm}<C&/}kx:+edFXsPU~#}3L|Moknoq;=');
define('SECURE_AUTH_KEY',  'd# >RQ9>wH^*yCcO}i8Y%H2irX--v$~+]l|iu=thDu8/|ks1D%+z>mz)*:[8yL.(');
define('LOGGED_IN_KEY',    'oOM>|I6W$c|y>^]dgH^G*^`fV&De*j!+fuG}ey4{v?~+vHs:LdtVF`6DztSdJP<h');
define('NONCE_KEY',        'qZbEokG/C}+zYJ)_??)1:]HCXaClK+>H~VA|C*3-- U{La^*0K&?s?DV r<?o0v}');
define('AUTH_SALT',        '2bm};UG*z1b#!pD:C](URP`(l*a=6|NU~Iju+1LR^=(:s.3O!Kbg4$@%dB+@[h]!');
define('SECURE_AUTH_SALT', '<++HZ0yl!?vuE>rk[6*Q@uvy;U})IL[YNeD;u,pDHq<e@(aJp|-xZOK[o&OI}@0D');
define('LOGGED_IN_SALT',   '?jJAM(kP*L>J0%SZ:ihFH1OIb8*jhUWnmmfFx-qT=e/lD;v>|ug;xE/UU++%x4w&');
define('NONCE_SALT',       'N--yFZ:9aT(c-@H%>3PA]CX]qmhc=o=@_asV_~c|+Dw_FggfwrHjzc%J)Xy_x1+~');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'aur_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
