		<hr class="dn" />
		
		<strong class="dn">Rodapé</strong>
		<footer class="rdp cem cf">
			<div class="centro">
				<strong class="dn">Menu Rodapé</strong>
				<nav class="m-rdp fl-lf cf">
					<?php wp_nav_menu(
						array(
							'theme_location' => 'menu_rdp',
							'container' => false,
							'menu_class' => '',
							'echo' => true,
							'depth' => 0,
							'walker' => ''
						)
					); ?>
				</nav>
				
				<div class="fb fl-rg">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					
					<div class="fb-like" data-href="https://www.facebook.com/pages/Aurorense-Parafusos/335221379938891?ref=ts&fref=ts" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>
				</div>
			</div>
			
			<div class="rdp-infos cem cf">
				<div class="centro cf">
					<address class="end fl-lf">
						<strong class="db">Grupo Aurorense</strong>
						<span class="db">Av. Augusto dos Anjos, 1713 - Bom Sucesso</span> 
						<span class="db">Fortaleza - Ceará - Brasil</span>
						<span class="db">Fone: (85) 3484.6533</span>
					</address>
					<!-- <address class="fone fl-lf">
						<strong class="f-num"><small>(85)</small> 3383.2256</strong>
					</address> -->
					<a href="http://www.e-deas.com.br" rel="external" title="Desenvolvido por E-deas Agência Web" class="edeas fl-rg img-rep">Desenvolvido por E-deas Agência Web</a>
				</div>
			</div>
		</footer>
		
		<!-- SCRIPTS -->
		<?php wp_footer(); ?>
		<!-- slide -->
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/jquery.cycle.all.min.js"></script>
		<script type="text/javascript">
			jQuery(function($){
			 $(".slide ul").cycle({
				fx: "fade",
				speed: 1000,
				timeout: 9000,
				pager:  "#nav"
			 });
			});
		</script>
		<!-- limpar no form -->
		<script type="text/javascript">
			function limparPadrao(campo) {if (campo.value == campo.defaultValue) {campo.value = "";}}
			function escreverPadrao(campo) {if (campo.value == "") {campo.value = campo.defaultValue;}}
		</script>
		<!-- target html -->
		<script type="text/javascript">jQuery(function($) {$('a[rel*=external]').click( function() {window.open(this.href); return false; });});</script>
		<!-- ir para o topo -->
		<script type="text/javascript">function goToByScroll(id){$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');}</script>
	</body>
</html>