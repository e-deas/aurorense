<?php
/* MINIATURAS */
add_theme_support( 'post-thumbnails' );
add_image_size( 'banner', 1600, 450, true );
add_image_size( 'noticia', 290, 90, true );
add_image_size( 'logos', 170, 70, true );
add_image_size( 'not-index', 180, 120, true );
add_image_size( 'not-int', 277, 300, true );
add_image_size( 'det-index', 972, 327, true );
add_image_size( 'produt-home', 225, 220, true );
add_image_size( 'produt-interno', 200, 195, true );
add_image_size( 'imagem-titulo', 1600, 160, true );
add_image_size( 'imagem-sidebar', 275, 0 );