<?php add_action('init', 'banner');
function banner() {
    $labels = array(
	    'name' => _x('Banner', 'post type general name'),
	    'singular_name' => _x('Banner', 'post type singular name'),
	    'add_new' => _x('Adicionar novo', 'Banner'),
	    'add_new_item' => __('Adicionar Banner'),
	    'edit_item' => __('Editar Banner'),
	    'new_item' => __('Novo Banner'),
	    'all_items' => __('Todos Banner'),
	    'view_item' => __('Ver Banner'),
	    'search_items' => __('Buscar Banner'),
	    'not_found' =>  __('Nenhum Banner encontrado'),
	    'not_found_in_trash' => __('Nenhum Banner na lixeira'),
	    'parent_item_colon' => '',
	    'menu_name' => 'Banner'
    );

    register_post_type( 'banner', array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
	    'has_archive' => 'banner',
	    'rewrite' => array(
		'slug' => 'banner',
		'with_front' => false,
	    ),
	    'capability_type' => 'post',
	    'has_archive' => false,
	    'hierarchical' => false,
	    'menu_position' => null,
	    'supports' => array('title','thumbnail')

	    )
    );
}

add_action('init', 'destaque');
function destaque() {
	$labels = array(
		'name'                => 'Destaque',
		'singular_name'       => 'Destaque',
		'menu_name'           => 'Destaque',
		'parent_item_colon'   => 'Destaque Pai:',
		'all_items'           => 'Todos Destaques',
		'view_item'           => 'Ver Destaque',
		'add_new_item'        => 'Adicionar Novo Destaque',
		'add_new'             => 'Novo Destaque',
		'edit_item'           => 'Editar Destaque',
		'update_item'         => 'Atualizar Destaque',
		'search_items'        => 'Buscar Destaque',
		'not_found'           => 'Nenhum Destaque Encontrado',
		'not_found_in_trash'  => 'Nenhum Destaque Encontrado na Lixeira',
	);
	$rewrite = array(
		'slug'                => 'destaque',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => false,
	);
	$args = array(
		'label'               => 'Destaque',
		'description'         => 'Destaque',
		'labels'              => $labels,
		'supports'            => array( 'title', /* 'editor', */ 'thumbnail' ),
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		#'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'query_var'           => 'destaque',
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'destaque', $args );
}


add_filter( 'gettext', 'change');
add_filter( 'ngettext', 'change');
function change ( $translated_text ){
	switch ( $translated_text ){
		case 'Posts' :
			$translated_text = __( 'Notícias' );
			break;
	} return $translated_text;
}



