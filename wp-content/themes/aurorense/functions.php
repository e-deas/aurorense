<?php
add_theme_support('automatic-feed-links');
get_template_part('includes/thumbs');
get_template_part('includes/posttype');
// get_template_part('includes/paged');
// get_template_part('includes/breadcrumbs');
/* SIDEBAR */ 
if ( function_exists('register_sidebar') )
register_sidebar(array(
	'name' => 'sidebar',
	'before_widget' => '<div id="%1$s" class="box_sidebar"><div class="cabecalho%1$s">',
	'after_widget' => '</div>',
	'before_title' => '</div><h3 class="txtIndent">',
	'after_title' => '</h3>',
));
/* MENU */
register_nav_menus( array(
	'menu' => __( 'Menu Principal' ),
	'menu_rdp' => __( 'Menu Rodapé' )
));
/* Limitar número caracteres conteúdo -------------- */
function except_limit($maximo) {
	$except = get_the_excerpt();
	if ( strlen($except) > $maximo ) {
		$continue = '...';
	}
	$except = mb_substr( $except, 0, $maximo, 'UTF-8' );
	echo $except.$continue;
}
/* ------------------------------------------------ */
// CUSTOM ADMIN MENU LINK FOR ALL SETTINGS
function all_settings_link() {
	 add_options_page(__('All Settings'), __('All Settings'), 'administrator', 'options.php'); 
}
add_action('admin_menu', 'all_settings_link');