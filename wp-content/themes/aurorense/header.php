<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="author" content="E-deas" />
		<meta name="description" content="<?php bloginfo('description'); ?>" />
		<meta name="keywords" content="" />

		<title><?php wp_title( '|', true, 'right' ); bloginfo('name');?></title>

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" />
		<link rel="apple-touch-icon" href="<?php bloginfo("template_url"); ?>/apple-touch-icon.png" />
		
		
		<?php wp_head(); ?>
		<link href="http://fonts.googleapis.com/css?family=Hammersmith+One" type="text/css" rel="stylesheet" />
		<link href="<?php bloginfo("template_url"); ?>/css/style.css" type="text/css" rel="stylesheet" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/forms-nr-min.css" />
		<!-- Grab Google CDN's jQuery. fall back to local if necessary
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script>!window.jQuery && document.write('<script src="/js/jquery-1.10.1.min.js"><\/script>')</script> -->
		<script src="<?php bloginfo("template_url"); ?>/js/script.js" type="text/javascript" charset="utf-8"></script>
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-51455133-1', 'aurorense.com.br');
		  ga('send', 'pageview');

		</script>
		
		<!--Start of Zopim Live Chat Script-->
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
		$.src='//v2.zopim.com/?29VUGEELrBuE7H6IybKR9PUFwP8L1iDg';z.t=+new Date;$.
		type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
		</script>
		<!--End of Zopim Live Chat Script-->
	</head>

	<body <?php body_class(); ?>>
		<header class="topo cem cf">
			<?php //$clima = array("1", "2", "3"); $rand_cli = array_rand($clima); ?>
			<!-- <img src="<?php //bloginfo("template_url"); ?>/images/bg-topo-rand-<?php //echo $clima[$rand_cli]; ?>.png" alt="Imagem ilustrativa do topo" class="bg-topo"> -->
			
			<div class="centro cf">
				<h1><a href="<?php echo home_url(); ?>" class="logo fl-lf img-rep" title="AURORENSE - Comércio e Indústria">AURORENSE - Comércio e Indústria</a></h1>
				
				<address class="fone">
					<strong class="f-num"><small>Horário de funcionamento</small></strong> <br class="dn" />
					<small class="db">Seg a Sex, das 07:30 às 17:30</small> <br class="dn" />
					<small class="db">Sáb, das 07:30 às 12:00</small>
				</address>
			</div>
			
			<hr class="dn" />
			
			<strong class="dn">Menu Principal</strong>
			<nav class="m-topo fl-lf cem cf">
				<div class="centro cf">
					<?php wp_nav_menu(
						array(
							'theme_location' => 'menu',
							'container'      => false,
							'menu_class'     => '',
							'echo'           => true,
							'depth'          => 0,
							'walker'         => ''
						)
					);?>
					
					<!-- <a href="#" title="Atendimento Online" class="bt-atendimento fl-rg">Atendimento Online</a> -->
				</div>
			</nav>
		</header>
		
		<hr class="dn" />