<?php

/*
Plugin Name: Tao Gallery
Plugin URI: http://www.bindigital.com.br
Description: Tao Gallery BinDigital
Version: 1.0
Author: Tiago Pires
Author URI: http://www.bindigital.com.br
*/


remove_shortcode('gallery');
add_shortcode('gallery', array('Tao_Gallery', 'parse_gallery'));
add_image_size( 'minigallery', 190, 150, true );


class Tao_Gallery
{
    public function install()
    {
    	
    }

    public function uninstall()
    {
    	
    }
	
	function scripts(){
		
	}
	
	function styles(){
		
	}
	
	public function parse_gallery($atts){
		global $post;
 
	    if ( ! empty( $atts['ids'] ) ) {
	        // 'ids' is explicitly ordered, unless you specify otherwise.
	        if ( empty( $atts['orderby'] ) )
	            $atts['orderby'] = 'post__in';
	        $atts['include'] = $atts['ids'];
	    }
	 
	 
	    extract(shortcode_atts(array(
	        'orderby' => 'menu_order ASC, ID ASC',
	        'include' => '',
	        'id' => $post->ID,
	        'itemtag' => 'dl',
	        'icontag' => 'dt',
	        'captiontag' => 'dd',
	        'columns' => 3,
	        'size' => 'minigallery',
	        'link' => 'file'
	    ), $atts));
	 
	 
	    $args = array(
	        'post_type' => 'attachment',
	        'post_status' => 'inherit',
	        'post_mime_type' => 'image',
	        'orderby' => $orderby
	    );

	    if ( !empty($include) )
	        $args['include'] = $include;
	    else {
	        $args['post_parent'] = $id;
	        $args['numberposts'] = -1;
	    }
	 
	    $images = get_posts($args);
		
		$taogallery = "<ul class='met-list'>";
		
	    foreach ( $images as $image ) {
	    	$taogallery .= "<li class='item'>";     
	        $caption = $image->post_excerpt;

	        $description = $image->post_content;
	        if($description == '') $description = $image->post_title;
	 
	        $image_alt = get_post_meta($image->ID,'_wp_attachment_image_alt', true);
	 
	        // render your gallery here
	        $taogallery .= wp_get_attachment_image($image->ID, $size);
	        //inserindo titulo na galeria
	        $taogallery .= "<strong class='tit'>".$caption."</strong>";
			$taogallery .= "</li>";
		}

		$taogallery .= "</ul>";
		
		return $taogallery;

	}
	
}

add_action( 'wp_enqueue_scripts', array('Tao_Gallery', 'scripts') );
add_action( 'wp_enqueue_style', array('Tao_Gallery', 'styles') );

# -----------------------------------------------------------------------------#
// Call Metode create Data Base
register_activation_hook(dirname(__FILE__) . DIRECTORY_SEPARATOR . basename(__FILE__), array('Tao_Gallery', 'install'));
// Call Metode drop Data Base
register_deactivation_hook(dirname(__FILE__) . DIRECTORY_SEPARATOR . basename(__FILE__), array('Tao_Gallery', 'uninstall'));

# -----------------------------------------------------------------------------#
