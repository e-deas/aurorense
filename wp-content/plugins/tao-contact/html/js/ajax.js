jQuery(document).ready(function($){
	$('#tao-contact .enviar').on('click', function(){
		validate('tao-contact');
		if(validateState){
			$('#tao-contact').append('<p class="sucess">Enviando...</p>');
			$.ajax({
			type      : 'post',
			url       : TaoContact.ajaxForm,
			data      : $("#tao-contact").serialize(),
			dataType  : 'html',
			success	  : function() {
				$('#tao-contact .sucess').text('Enviado com sucesso');
				$('#tao-contact input[type="text"], #tao-contact input[type="email"], #tao-contact textarea').val('');
				setTimeout(function() {
					$('#tao-contact .sucess').fadeOut();
				}, 1500);
			}
			});
		}
    });
});