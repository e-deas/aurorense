<?php

/*
Plugin Name: Tao Contact
Plugin URI: http://www.bindigital.com.br
Description: Tao Contact Ajax BinDigital
Version: 1.0
Author: Tiago Pires
Author URI: http://www.bindigital.com.br
*/


/* CHAMADAS */
/* Styles */
add_action( 'wp_enqueue_scripts', 'callform' );
function callform(){
/* Scripts */
	wp_enqueue_script( "validate", plugin_dir_url( __FILE__ ) . "html/js/validate.js", array('jquery') );
	wp_enqueue_script( "ajax-contact", plugin_dir_url( __FILE__ ) . "html/js/ajax.js" );
	wp_localize_script( "ajax-contact", "TaoContact", array( 'ajaxForm' => admin_url( 'admin-ajax.php?action=contact' )) );
}

class Tao_Contact
{
    public function form_submit()
	{
	    $nome = $_POST['nome'];
	    $email = $_POST['email'];
		$mensagem = $_POST['mensagem'];
	    $site = get_bloginfo('name');
		
		$email_remetente = $email;
		$headers = "MIME-Version: 1.1\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1\n";
		$headers .= "From: $email\n"; // remetente
		$headers .= "Return-Path: $email\n"; // return-path
		$headers .= "Reply-To: $email\n"; // Endereço (devidamente validado) que o seu usuário informou no contato
		$envio = mail("contato@aurorense.com.br", "Enviado por $nome de WebSite $site", $mensagem, $headers, "-f$email");
	}
}

# -----------------------------------------------------------------------------#
// Function Save Form
add_action('wp_ajax_contact', array('Tao_Contact', 'form_submit'));
add_action('wp_ajax_nopriv_contact', array('Tao_Contact', 'form_submit'));

# -----------------------------------------------------------------------------#
// Form Cadastre Front-end
class Tao_Form_Contact extends WP_Widget {

	public function __construct() {
		//Identificador do widget
    	$id_base = 'tao_contact';
    	//Nome do Widget que será exibido
    	$name = 'Formulário Contato';
    	//Adicionado Descrição do widget
    	$widget_options = array('description' => 'Formulário de Contato');

		parent::__construct($id_base, $name, $widget_options);
	}
	
	#public function form($instance) {}
	
	#public function update($new_instance, $old_instance) {}
	
	public function widget($args, $instance) {
		require_once (dirname(__FILE__).'/html/form-contato.php');
	}
	
}


//Função para registar o widget
function tao_contact(){
    //Registra o widget que criamos
     register_widget('Tao_Form_Contact');
}

//Com função add_action, atribuimos uma função, usando o gancho widgets_init
add_action('widgets_init', 'tao_contact');

// Insira em uma pagina o seguinte codigo [form_cadastro] para que seja exibido o formulario
add_shortcode('form_contato', array( 'Tao_Form_Contact', 'widget' ));